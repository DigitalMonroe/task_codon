from django.shortcuts import render
from .forms import DnaCheckForm
from .find_coton import find_coton


def index(request):
    template_name = 'finder/index.html'
    DNA = [
            'tgacccactaatcagcaacatagcactttgagcaaaggcctgtgttggagctattggccc',
            'caaaactgcctttccctaaacagtgttcaccattgtagacctcaccactgttcgcgtaac',
            'aactggcatgtcctgggggttaatactcac'
        ]

    message = ''
    success = False

    if request.method == 'POST':
        form = DnaCheckForm(request.POST)
        if form.is_valid():
            user_coton = request.POST.get('user_coton')
            if find_coton(user_coton, DNA):
                message = f'Котон {user_coton} найден!'
                success = True
            else:
                message = f'Котон {user_coton} не найден!'
        else:
            message = "Некорректная форма!"
    else:
        form = DnaCheckForm()

    context = {
        'title': 'Главная',
        'form': form,
        'message': message,
        'success': success,
    }

    return render(request, template_name, context=context)
