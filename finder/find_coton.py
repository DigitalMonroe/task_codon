
def find_coton(search: str, DNA: list):
    """Search string in DNA list with recursion"""
    hight = len(DNA) - 1
    mid = (hight + 1) // 2

    if len(DNA) <= 0:
        return False

    if search in DNA[mid]:
        return True

    else:
        return find_coton(search, DNA[:mid-1]) or \
            find_coton(search, DNA[mid+1:])


def linear_finder(search: str, DNA: list):
    for dna in DNA:
        if search in dna:
            return True

    return False
